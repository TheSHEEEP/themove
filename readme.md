[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](license.md)

# Moving Out Tool

This is a multi-platform program consisting of a front-end (working in all HTML5, flash & native platforms) and a backend (PHP & MySQL), all of which were coded (using Haxe) in just a few days of work.  
I wrote this to help some flat mates & myself distributing our furniture, etc. when moving out.

This tool is meant as a small helper between some friends to organize moving out a bit better.  
If you want a version with more features and better security, feel free to contact me: [jan@jdrabner.eu][e8645982]

  [e8645982]: mailto:jan@jdrabner.eu "Jan's email address."

## License

It is MIT!
So... enjoy ;)
