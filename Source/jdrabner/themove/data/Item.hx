package jdrabner.themove.data;

import jdrabner.themove.utils.TextUtils;
import haxe.ds.IntMap;

/**
 * The data that belongs to an item.
 * @type {[type]}
 */
class Item
{
    public var id           : Int = 0;
    public var name         : String = "";
    public var description  : String = "";
    public var createdBy   : Int = 0;
    public var numComments  : Int = 0;
    public var statuses     : IntMap<Globals.ItemUserStatus> = new IntMap<Globals.ItemUserStatus>();

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new() {}

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Convenience debug function.
     * @return {String}
     */
    public function toString() : String
    {
        return '$id/$name/$description/$createdBy/$statuses';
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Returns true if there is a conflict in this item (at least 2 persons want it).
     * @return {Bool}
     */
    public function isConflicted() : Bool
    {
        var someOneWants : Bool = false;
        for (userId in statuses.keys())
        {
            if (statuses.get(userId) == Globals.ItemUserStatus.Wants)
            {
                if (someOneWants) return true;
                someOneWants = true;
            }
        }

        return false;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Returns true if the item is available (nobody wants it).
     * @return {Bool}
     */
    public function isAvailable() : Bool
    {
        for (userId in statuses.keys())
        {
            if (statuses.get(userId) == Globals.ItemUserStatus.Wants)
            {
                return false;
            }
        }

        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Returns true if this item requires a decision by the passed user.
     * @param  {Int}  p_userId The user ID to check for.
     * @return {Bool}
     */
    public function requiresDecision(p_userId : Int) : Bool
    {
        if (statuses.exists(p_userId))
        {
            if (statuses.get(p_userId) != Globals.ItemUserStatus.Nothing)
            {
                return false;
            }
        }

        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Creates an Item from the passed dynamic data.
     * @param  {Dynamic} p_dyn The data dyn to use.
     * @return {Item}
     */
    public static function fromDynamic(p_dyn : Dynamic) : Item
    {
        var retVal : Item = new Item();
        retVal.id = p_dyn.id;
        retVal.name = TextUtils.decodeFromServerFormat(p_dyn.name);
        retVal.description = TextUtils.decodeFromServerFormat(p_dyn.description);
        retVal.createdBy = p_dyn.createdBy;
        retVal.numComments = p_dyn.numComments;
        for (status in cast(p_dyn.statuses, Array<Dynamic>))
        {
            if (status.id != null && status.status != null)
            {
                retVal.statuses.set(status.id, status.status);
            }
        }
        return retVal;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Sorting function by ID, ascending.
     * @param  {Item} p_left  The left item.
     * @param  {Item} p_right The right item.
     * @return {Int} 0 if left == right, a positive Int if left < right and a negative Int if left > right.
     */
    public static function sortByIdAsc(p_left : Item, p_right : Item) : Int
    {
        if (p_left.id == p_right.id) return 0;
        return (p_left.id < p_right.id)? 1 : -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Sorting function by ID, descending
     * @param  {Item} p_left  The left item.
     * @param  {Item} p_right The right item.
     * @return {Int} 0 if left == right, a positive Int if left > right and a negative Int if left < right.
     */
    public static function sortByIdDesc(p_left : Item, p_right : Item) : Int
    {
        if (p_left.id == p_right.id) return 0;
        return (p_left.id > p_right.id)? 1 : -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Sorting function by name, ascending.
     * @param  {Item} p_left  The left item.
     * @param  {Item} p_right The right item.
     * @return {Int} 0 if left == right, a positive Int if left < right and a negative Int if left > right.
     */
    public static function sortByNameAsc(p_left : Item, p_right : Item) : Int
    {
        if (p_left.name.toLowerCase() == p_right.name.toLowerCase()) return 0;
        return (p_left.name.toLowerCase() < p_right.name.toLowerCase())? 1 : -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Sorting function by name, descending
     * @param  {Item} p_left  The left item.
     * @param  {Item} p_right The right item.
     * @return {Int} 0 if left == right, a positive Int if left > right and a negative Int if left < right.
     */
    public static function sortByNameDesc(p_left : Item, p_right : Item) : Int
    {
        if (p_left.name.toLowerCase() == p_right.name.toLowerCase()) return 0;
        return (p_left.name.toLowerCase() > p_right.name.toLowerCase())? 1 : -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Sorting function by current user's status, descending.
     * @param  {Item} p_left  The left item.
     * @param  {Item} p_right The right item.
     * @return {Int} 0 if left == right, a positive Int if left < right and a negative Int if left > right.
     */
    public static function sortByStatusDesc(p_left : Item, p_right : Item) : Int
    {
        var statusLeft : Globals.ItemUserStatus = Globals.ItemUserStatus.Nothing;
        var statusRight : Globals.ItemUserStatus = Globals.ItemUserStatus.Nothing;
        if (p_left.statuses.exists(Globals.userId))
        {
            statusLeft = p_left.statuses.get(Globals.userId);
        }
        if (p_right.statuses.exists(Globals.userId))
        {
            statusRight = p_right.statuses.get(Globals.userId);
        }

        if (statusLeft == statusRight) return 0;
        if (statusRight == Globals.ItemUserStatus.Nothing) return -1;
        switch(statusLeft)
        {
            case Globals.ItemUserStatus.Nothing:
                return 1;
            case Globals.ItemUserStatus.DoesntWant:
                return 1;
            case Globals.ItemUserStatus.NotSure:
                if (statusRight == Globals.ItemUserStatus.DoesntWant)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            case Globals.ItemUserStatus.Wants:
                return -1;
            case Globals.ItemUserStatus.Trash:
                if (statusRight == Globals.ItemUserStatus.Nothing)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
        }
    }
}
