package jdrabner.themove.data;

import haxe.ds.StringMap;
import haxe.ds.IntMap;

/**
 * Globals class to help with some often needed variables.
 * @type {[type]}
 */
class Globals
{
    public static var serverAddress : String = "";
    public static var userId        : Int = 0;
    public static var user          : String = "";
    public static var pass          : String = "";

    public static var numUsers          : Int = 0;
    public static var availableUsers    : IntMap<String> = new IntMap<String>();

    public static var allItems  : IntMap<Item> = new IntMap<Item>();
}

@:enum
abstract ItemUserStatus(Int) {
  var Nothing = 0;
  var Wants = 1;
  var DoesntWant = 2;
  var NotSure = 3;
  var Trash = 4;
}
