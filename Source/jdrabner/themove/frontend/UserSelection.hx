package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.PopupManager;
import haxe.ui.toolkit.controls.popups.Popup;
import haxe.ui.toolkit.controls.popups.PopupContent;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.TextInput;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import haxe.ui.toolkit.controls.selection.ListSelector;
import firetongue.FireTongue;
import openfl.events.EventDispatcher;
import openfl.events.KeyboardEvent;
import openfl.ui.Keyboard;

/**
 * Selects the user and checks the password.
 * @type {[type]}
 */
class UserSelection extends EventDispatcher
{
    // Custom events
    public static inline var LOGGED_IN : String = "Logged in";

    private var _container  : VBox = null;

    private var _userLabel  : Text = null;
    private var _user       : ListSelector = null;
    private var _passLabel  : Text = null;
    private var _pass       : TextInput = null;
    private var _button     : Button = null;

    private var _popup      : Popup = null;

    private var _passText   : String = "";

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();

        // Container
        _container = new VBox();
        _container.percentWidth = 100.0;

        // User selection
        var userRow : HBox = new HBox();
        userRow.percentWidth = 100.0;
        _userLabel = new Text();
        _userLabel.percentWidth = 30.0;
        _userLabel.text = Main.tongue.get("$USER");
        _userLabel.verticalAlign = "center";
        _user = new ListSelector();
        _user.percentWidth = 70.0;
        _user.verticalAlign = "center";
        userRow.addChild(_userLabel);
        userRow.addChild(_user);

        // Pass entry
        var passRow : HBox = new HBox();
        passRow.percentWidth = 100.0;
        _passLabel = new Text();
        _passLabel.percentWidth = 30.0;
        _passLabel.text = Main.tongue.get("$PASSWORD");
        _passLabel.verticalAlign = "center";
        _pass = new TextInput();
        _pass.percentWidth = 70.0;
        _pass.verticalAlign = "center";
        _pass.displayAsPassword = true;
        passRow.addChild(_passLabel);
        passRow.addChild(_pass);

        // Button
        _button = new Button();
        _button.text = Main.tongue.get("$LOGIN");
        _button.addEventListener(UIEvent.CLICK, handleLogin);
        _button.horizontalAlign = "center";

        // Add all and mind dimensions
        var sep1 : VSplitter = new VSplitter();
        var sep2 : VSplitter = new VSplitter();
        sep1.height = 10.0;
        sep2.height = 10.0;
        _container.addChild(userRow);
        _container.addChild(sep1);
        _container.addChild(passRow);
        _container.addChild(sep2);
        _container.addChild(_button);

        // Get the users
        var callUrl : String = '${Globals.serverAddress}/themove/login.php?do=getUsers';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = handleUserData;
        http.onError = function (p_msg : String) : Void {
            trace("Error:");
            trace(p_msg);
        };
        http.request(false);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles the user data incoming from the call to the server.
     * @param  {String} p_data The incoming data in JSON format.
     * @return {Void}
     */
    private function handleUserData (p_data : String) : Void
    {
        var items : Array<Dynamic> = haxe.Json.parse(p_data);
        for (item in items)
        {
            _user.dataSource.add({text: item.name});
            Globals.availableUsers.set(item.id, item.name);
            Globals.numUsers++;
        }
        _user.selectedIndex = 0;
        _user.autoSize = true;
        _user.layout.refresh();

        // Show the popup
        _container.autoSize = true;
        _container.layout.refresh();
        _popup = PopupManager.instance.showCustom(_container, Main.tongue.get("$USERSELECTION"), {
            buttons: {},
            id: "popupContainer"
        });
        _popup.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
        _popup.height = _container.height;
        _user.layout.refresh();
        _user.showList();
        _user.hideList();
    };

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles the login.
     * @param  {UIEvent} p_event The passed event.
     * @return {Void}
     */
    private function handleLogin(p_event : UIEvent) : Void
    {
        // Gather user & pass
        var user : String = _user.text;
        var pass : String = _pass.text;
        var callUrl : String = '${Globals.serverAddress}/themove/login.php?do=login&user=$user&pass=$pass';

        // Remember the user & pass for later use
        Globals.user = user;
        Globals.pass = pass;
        for (key in Globals.availableUsers.keys())
        {
            if (Globals.availableUsers.get(key) == user)
            {
                Globals.userId = key;
            }
        }

        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = handleLoginResult;
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(Main.tongue.get("$COULD_NOT_CONNECT"), Main.tongue.get("$ERROR"));
            _button.text = Main.tongue.get("$LOGIN");
        };
        _button.text = Main.tongue.get("$LOGGING_IN");
        http.request(false);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will determine if the login was successful and act accordingly.
     * @param  {String} p_data The data passed from the server.
     * @return {Void}
     */
    private function handleLoginResult(p_data : String) : Void
    {
        var result : Bool = p_data == "true";
        if (!result)
        {
            PopupManager.instance.showSimple(Main.tongue.get("$WRONG_PASSWORD"), Main.tongue.get("$ERROR"));
        }
        else
        {
            PopupManager.instance.hidePopup(_popup);
            dispatchEvent(new UIEvent(LOGGED_IN));
        }
        _button.text = Main.tongue.get("$LOGIN");
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles each pressed key.
     * @param  {KeyboardEvent} p_event The event data.
     * @return {Void}
     */
    private function handleKeyDown(p_event : KeyboardEvent) : Void
    {
        if (p_event.keyCode == Keyboard.ENTER)
        {
            handleLogin(null);
        }
    }
}
