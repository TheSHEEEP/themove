package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import jdrabner.themove.utils.TextUtils;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.controls.popups.Popup;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.TextInput;
import haxe.ui.toolkit.core.PopupManager;
import haxe.ui.toolkit.core.base.HorizontalAlign;
import haxe.ui.toolkit.core.base.VerticalAlign;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import firetongue.FireTongue;

/**
 * All global actions (like adding/removing an item).
 * @type {[type]}
 */
class Actions extends Container
{
    public static inline var  ITEM_ADDED        : String = "Item added";
    public static inline var  ITEM_REMOVED      : String = "Item removed";
    public static inline var  REQUEST_UPDATE    : String = "Update requested";
    public static inline var  SAVE_LIST         : String = "Save list";

    private var _refresh            : Button = null;

    private var _addContainer   : VBox = null;
    private var _addItemInput   : TextInput = null;
    private var _addItemOk      : Button = null;
    private var _addPopup       : Popup = null;
    private var _addItem        : Button = null;

    private var _removeContainer   : VBox = null;
    private var _removeItemOk      : Button = null;
    private var _removePopup       : Popup = null;
    private var _removeItem        : Button = null;

    private var _printList      : Button = null;

    private var _setUnwanted    : Button = null;

    private var _currentItem    : Item = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
        layout = new VerticalLayout();
        percentHeight = 30.0;
        percentWidth = 100.0;

        var sep0 : VSplitter = new VSplitter();
        sep0.percentWidth = 100.0;
        sep0.percentHeight = 10.0;
        addChild(sep0);

        // Refresh
        _refresh = new Button();
        _refresh.horizontalAlign = "center";
        _refresh.text = Main.tongue.get("$UPDATE");
        _refresh.addEventListener(UIEvent.CLICK, handleRefreshClick);
        addChild(_refresh);

        // Splitter
        var sep1 : VSplitter = new VSplitter();
        sep1.percentWidth = 100.0;
        sep1.percentHeight = 10.0;
        addChild(sep1);

        // Add item
        _addItem = new Button();
        _addItem.text = Main.tongue.get("$ADDITEM");
        _addItem.addEventListener(UIEvent.CLICK, handleAddItem);
        _addItem.horizontalAlign = "center";
        addChild(_addItem);

        // Create the add item popup container
        _addContainer = new VBox();
        _addContainer.percentWidth = 100.0;
        var row : HBox = new HBox();
        row.percentWidth = 100.0;
        var label : Text = new Text();
        label.text = Main.tongue.get("$ITEMNAME");
        label.percentWidth = 30.0;
        row.addChild(label);
        _addItemInput = new TextInput();
        _addItemInput.percentWidth = 70.0;
        row.addChild(_addItemInput);
        var row2 : HBox = new HBox();
        row2.percentWidth = 70.0;
        row2.horizontalAlign = "center";
        _addItemOk = new Button();
        _addItemOk.text = Main.tongue.get("$OK");
        _addItemOk.percentWidth = 40.0;
        _addItemOk.horizontalAlign = "left";
        row2.addChild(_addItemOk);
        var buttonCancel : Button = new Button();
        buttonCancel.text = Main.tongue.get("$CANCEL");
        buttonCancel.percentWidth = 40.0;
        buttonCancel.horizontalAlign = "right";
        row2.addChild(buttonCancel);
        _addContainer.addChild(row);
        _addContainer.addChild(row2);

        // Splitter
        var sep2 : VSplitter = new VSplitter();
        sep2.percentWidth = 100.0;
        sep2.percentHeight = 10.0;
        addChild(sep2);

        // Remove item
        _removeItem = new Button();
        _removeItem.text = Main.tongue.get("$REMOVEITEM");
        _removeItem.addEventListener(UIEvent.CLICK, handleRemoveItem);
        _removeItem.horizontalAlign = "center";
        _removeItem.disabled = true;
        addChild(_removeItem);

        // Create the remove item popup container
        _removeContainer = new VBox();
        _removeContainer.percentWidth = 100.0;
        label = new Text();
        label.multiline = true;
        label.wrapLines = true;
        label.text = Main.tongue.get("$CONFIRM_DELETION");
        label.percentWidth = 100.0;
        _removeContainer.addChild(label);
        row = new HBox();
        row.percentWidth = 70.0;
        row.horizontalAlign = "center";
        _removeItemOk = new Button();
        _removeItemOk.text = Main.tongue.get("$OK");
        _removeItemOk.percentWidth = 40.0;
        _removeItemOk.horizontalAlign = "left";
        row.addChild(_removeItemOk);
        buttonCancel = new Button();
        buttonCancel.text = Main.tongue.get("$CANCEL");
        buttonCancel.percentWidth = 40.0;
        buttonCancel.horizontalAlign = "right";
        row.addChild(buttonCancel);
        _removeContainer.addChild(row);

        // Splitter
        var sep3 : VSplitter = new VSplitter();
        sep3.percentWidth = 100.0;
        sep3.percentHeight = 10.0;
        addChild(sep3);

        // Print the current list
        _printList = new Button();
        _printList.text = Main.tongue.get("$SAVE_LIST");
        _printList.addEventListener(UIEvent.CLICK, function(p_event : UIEvent) {
            dispatchEvent(new UIEvent(SAVE_LIST));
        });
        _printList.horizontalAlign = "center";
        addChild(_printList);

        // Splitter
        var sep4 : VSplitter = new VSplitter();
        sep4.percentWidth = 100.0;
        sep4.percentHeight = 10.0;
        addChild(sep4);

        // Set all unset items to not wanted
        _setUnwanted = new Button();
        _setUnwanted.text = Main.tongue.get("$SET_UNWANTED");
        _setUnwanted.addEventListener(UIEvent.CLICK, handleSetUnwanted);
        _setUnwanted.horizontalAlign = "center";
        addChild(_setUnwanted);

        autoSize = true;
        layout.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will show the popup to add an item.
     * @param  {UIEvent} p_event Event data.
     * @return {Void}
     */
    private function handleAddItem(p_event : UIEvent) : Void
    {
        // Show the popup asking for the name
        var theclone : VBox = _addContainer.clone();
        _addItemOk = cast cast(theclone.getChildAt(1), Container).getChildAt(0);
        _addItemOk.addEventListener(UIEvent.CLICK, doAddItem);
        _addItemInput = cast cast(theclone.getChildAt(0), Container).getChildAt(1);
        cast(theclone.getChildAt(1), Container).getChildAt(1).addEventListener(UIEvent.CLICK, function (p_event : UIEvent) : Void {
            PopupManager.instance.hidePopup(_addPopup);
        });
        _addPopup = PopupManager.instance.showCustom(theclone, Main.tongue.get("$ADDITEM"), {
            buttons: {},
            id: "popupContainer"
        });
        _addPopup.height = _addContainer.height;
        _addItemInput.focus();
        _addItemOk.disabled = false;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will try to add an item to the list.
     * @param  {UIEvent} p_event Event data.
     * @return {Void}
     */
    private function doAddItem(p_event : UIEvent) : Void
    {
        var user : String = Globals.user;
        var pass : String = Globals.pass;
        var name : String = TextUtils.encodeToServerFormat(_addItemInput.text);
        if (name == "")
        {
            PopupManager.instance.showSimple(Main.tongue.get("$NO_EMPY_ALLOWED"), Main.tongue.get("$ERROR"));
            return;
        }

        var callUrl : String = '${Globals.serverAddress}/themove/addItem.php?user=$user&pass=$pass&name=$name';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = handleAddItemResult;
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(Main.tongue.get("$COULD_NOT_CONNECT"), Main.tongue.get("$ERROR"));
        };
        http.request(false);
        _addItemOk.disabled = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles the server result of the add item attemp.
     * @param  {String} p_data The data passed by the server.
     * @return {Void}
     */
    private function handleAddItemResult(p_data : String) : Void
    {
        PopupManager.instance.hidePopup(_addPopup);
        dispatchEvent(new UIEvent(ITEM_ADDED));
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Remember the item currently selected.
     * @param  {Int}  p_id [description]
     * @return {Void}
     */
    public function setCurrentItem(p_id : Int) : Void
    {
        if (p_id == -1)
        {
            _currentItem = null;
            _removeItem.disabled = true;
        }
        else
        {
            _currentItem = Globals.allItems.get(p_id);
            _removeItem.disabled = false;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will show the popup to remove an item (if an item is selected).
     * @param  {UIEvent} p_event Event data.
     * @return {Void}
     */
    private function handleRemoveItem(p_event : UIEvent) : Void
    {
        if (_currentItem == null)
        {
            return;
        }

        // Show the popup asking again
        var theclone : VBox = _removeContainer.clone();
        _removeItemOk = cast cast(theclone.getChildAt(1), Container).getChildAt(0);
        _removeItemOk.addEventListener(UIEvent.CLICK, doRemoveItem);
        cast(theclone.getChildAt(1), Container).getChildAt(1).addEventListener(UIEvent.CLICK, function (p_event : UIEvent) : Void {
            PopupManager.instance.hidePopup(_removePopup);
        });
        _removePopup = PopupManager.instance.showCustom(theclone, Main.tongue.get("$PLEASE_CONFIRM"), {
            buttons: {},
            id: "popupContainer"
        });
        _removePopup.height = _removeContainer.height;
        _removeItemOk.disabled = false;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will try to remove an item to the list.
     * @param  {UIEvent} p_event Event data.
     * @return {Void}
     */
    private function doRemoveItem(p_event : UIEvent) : Void
    {
        var user : String = Globals.user;
        var pass : String = Globals.pass;
        var id : Int = _currentItem.id;

        var callUrl : String = '${Globals.serverAddress}/themove/removeItem.php?user=$user&pass=$pass&id=$id';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = handleRemoveItemResult;
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(Main.tongue.get("$COULD_NOT_CONNECT"), Main.tongue.get("$ERROR"));
            cast(_removeContainer.getChildAt(1), Container).disabled = false;
        };
        http.request(false);
        _removeItemOk.disabled = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles the server result of the remove item attemp.
     * @param  {String} p_data The data passed by the server.
     * @return {Void}
     */
    private function handleRemoveItemResult(p_data : String) : Void
    {
        PopupManager.instance.hidePopup(_removePopup);
        cast(_removeContainer.getChildAt(1), Container).disabled = false;

        if (Globals.user == "Guest")
        {
            return;
        }
        var event : UIEvent = new UIEvent(ITEM_REMOVED);
        event.data = haxe.Json.parse(p_data).id;
        dispatchEvent(event);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will download the newest data.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleRefreshClick(p_event : UIEvent) : Void
    {
        dispatchEvent(new UIEvent(REQUEST_UPDATE));
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will set all without selection to unwanted for the current user.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleSetUnwanted(p_event : UIEvent) : Void
    {
        var user : String = Globals.user;
        var pass : String = Globals.pass;

        var callUrl : String = '${Globals.serverAddress}/themove/setUnsetUnwanted.php?user=$user&pass=$pass';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(Main.tongue.get("$COULD_NOT_CONNECT"), Main.tongue.get("$ERROR") + ":handleSetUnwanted");
        };
        http.request(false);
    }
}
