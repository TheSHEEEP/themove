package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import openfl.display.Sprite;
import openfl.Assets;
import openfl.events.MouseEvent;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.style.StyleManager;
import haxe.ui.toolkit.style.Style;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Component;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.Divider;
import haxe.ui.toolkit.controls.selection.ListSelector;
import haxe.ui.toolkit.layout.HorizontalLayout;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.themes.GradientTheme;
import haxe.ui.toolkit.events.UIEvent;
import firetongue.FireTongue;

/**
 * Main frontend class.
 * @type {[type]}
 */
class Main extends Sprite
{
    static public var tongue    : FireTongue = null;

    private var _mainBox    : HBox = null;
    private var _leftBox    : VBox = null;
    private var _rightBox    : VBox = null;

    private var _actions         : Actions = null;
    private var _comments         : Comments = null;
    private var _itemList        : ItemList = null;
    private var _itemDetails     : ItemDetails = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new ()
    {
        super ();

        Globals.serverAddress = "http://185.17.144.219/demo";

        // Init firetongue
        tongue = new FireTongue();
        tongue.init("en-US", onFireTongueLoaded);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Construct the entire screen.
     * @param  {Root} p_root The root to add everything to.
     * @return {Void}
     */
    private function setup(p_root : Root) : Void
    {
        // Construct the main screen by putting the single parts in the right place
        p_root.percentHeight = 100.0;
        p_root.percentWidth = 100.0;

        // Boxes
        _mainBox = new HBox();
        _mainBox.layout = new HorizontalLayout();
        _mainBox.percentWidth = 100.0;
        _mainBox.percentHeight = 100.0;
        _leftBox = new VBox();
        _leftBox.layout = new VerticalLayout();
        _leftBox.percentHeight = 100.0;
        _leftBox.percentWidth = 70.0;
        var sep0 : Component = new Component();
        sep0.percentHeight = 100.0;
        sep0.width = 2;
        sep0.id = "vertLine";
        _rightBox = new VBox();
        _rightBox.layout = new VerticalLayout();
        _rightBox.percentHeight = 100.0;
        _rightBox.percentWidth = 30.0;
        _mainBox.addChild(_leftBox);
        _mainBox.addChild(sep0);
        _mainBox.addChild(_rightBox);
        p_root.addChild(_mainBox);

        // Parts
        _actions = new Actions();
        _actions.addEventListener(Actions.ITEM_ADDED, causeListRefresh);
        _actions.addEventListener(Actions.ITEM_REMOVED, handleItemRemoved);
        _actions.addEventListener(Actions.REQUEST_UPDATE, causeListRefresh);
        _actions.addEventListener(Actions.SAVE_LIST, saveCurrentList);
        var sep1 : Divider = new Divider();
        _comments = new Comments();
        _comments.addEventListener(Comments.COMMENT_SENT, handleItemUpdated);
        _itemList = new ItemList();
        _itemList.addEventListener(ItemList.ITEM_SELECTED, handleItemSelected);
        var sep2 : Divider = new Divider();
        _itemDetails = new ItemDetails();
        _itemDetails.addEventListener(ItemDetails.ITEM_UPDATED, handleItemUpdated);
        _rightBox.addChild(_actions);
        _rightBox.addChild(sep1);
        _rightBox.addChild(_comments);
        _leftBox.addChild(_itemList);
        _leftBox.addChild(sep2);
        _leftBox.addChild(_itemDetails);

        _leftBox.autoSize = true;
        _leftBox.layout.refresh();
        _rightBox.autoSize = true;
        _rightBox.layout.refresh();

        _mainBox.autoSize = true;
        _mainBox.layout.refresh();

        // _mainBox.disabled = true;

        // Show user selection]
        var userSel : UserSelection = new UserSelection();
        userSel.addEventListener(UserSelection.LOGGED_IN, handleLoggedIn);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Called when firetongue did load its files.
     * @return {Void}
     */
    private function onFireTongueLoaded() : Void
    {
        Toolkit.theme = new GradientTheme();
        StyleManager.instance.addStyle("#vertLine", new Style({
            width: 1,
            backgroundColor: 0x3f4c6b
        }));
        StyleManager.instance.addStyle("Text", new Style({
            fontSize: 13,
            fontName: "fonts/Oxygen"
        }));
        StyleManager.instance.addStyle("#listEntry", new Style({
            paddingTop : 2,
            paddingBottom : 2,
            paddingLeft : 0,
            paddingRight : 2,
            spacingY : 0
        }));
        StyleManager.instance.addStyle("#popupContainer", new Style({
            backgroundColor: 0xE9EDF5,
            borderColor: 0x495267,
            borderSize: 1,
            cornerRadius: 3,
            paddingLeft: 2,
            paddingRight: 2,
            paddingTop: 2,
            paddingBottom: 2,
            spacing: 1
        }));
        StyleManager.instance.addStyle("#selectedEntry", new Style({
            backgroundColor: 0xE9EDF5
        }));
        Toolkit.init();
        Toolkit.openFullscreen(setup);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles the login of the user.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleLoggedIn(p_event : UIEvent) : Void
    {
        _mainBox.disabled = false;
        _itemList.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will cause the main item list to refresh.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function causeListRefresh(p_event : UIEvent) : Void
    {
        _itemList.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will notify UI parts that a new item has been selected.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleItemSelected(p_event : UIEvent) : Void
    {
        var id : Int = p_event.data;

        _comments.setItem(id);
        _itemDetails.setItem(id);
        _actions.setCurrentItem(id);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will update the item's display.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleItemUpdated(p_event : UIEvent) : Void
    {
        _itemList.updateItemDisplay(p_event.data);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will reset the display parts.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleItemRemoved(p_event : UIEvent) : Void
    {
        var id : Int = p_event.data;

        // Remove the ID from the globals
        Globals.allItems.remove(id);

        // Update all displays
        causeListRefresh(p_event);
        _itemList.notifyRemovedItem(id);
        _comments.notifyRemovedItem(id);
        _itemDetails.notifyRemovedItem(id);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will save the currently visible list.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function saveCurrentList(p_event : UIEvent) : Void
    {
        _itemList.saveCurrent();
    }

}
