package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import jdrabner.themove.utils.TextUtils;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.containers.ScrollView;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.TextInput;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import haxe.ui.toolkit.core.PopupManager;
import firetongue.FireTongue;

/**
 * Manages the display and addition of comments.
 * @type {[type]}
 */
class Comments extends VBox
{
    public static inline var COMMENT_SENT   : String = "Comment sent";

    private var _refItem        : Item = null;
    private var _lastSentText   : String = "öäü";

    private var _topLabel           : Text = null;
    private var _input              : TextInput = null;
    private var _commentsScroll     : ScrollView = null;
    private var _commentsContainer  : VBox = null;
    private var _sendButton         : Button = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
        percentHeight = 70.0;
        percentWidth = 100.0;

        // Top label
        _topLabel = new Text();
        _topLabel.text = Main.tongue.get("$COMMENTSTOP");
        _topLabel.id = "title";
        _topLabel.style.fontSize = 16.0;
        _topLabel.style.fontBold = true;
        _topLabel.percentWidth = 100.0;
        _topLabel.percentHeight = 5.0;
        _topLabel.horizontalAlign = "center";
        addChild(_topLabel);

        // Scrolled comment view
        _commentsScroll = new ScrollView();
        _commentsScroll.percentWidth = 100.0;
        _commentsScroll.percentHeight = 67.0;
        _commentsScroll.style.backgroundColor = 0xFEFEFE;
        addChild(_commentsScroll);

        // Container of comments
        _commentsContainer = new VBox();
        _commentsContainer.percentWidth = 100.0;
        _commentsContainer.autoSize = true;
        _commentsScroll.addChild(_commentsContainer);

        // Input box
        _input = new TextInput();
        _input.percentWidth = 100.0;
        _input.percentHeight = 20.0;
        _input.multiline = true;
        _input.wrapLines = true;
        _input.horizontalAlign = "center";
        addChild(_input);

        // Send button
        _sendButton = new Button();
        _sendButton.percentWidth = 50.0;
        _sendButton.percentHeight = 5.0;
        _sendButton.horizontalAlign = "center";
        _sendButton.text = Main.tongue.get("$SEND_COMMENT");
        _sendButton.addEventListener(UIEvent.CLICK, handleSendComment);
        addChild(_sendButton);

        // Splitter
        var split : VSplitter = new VSplitter();
        split.percentWidth = 100.0;
        split.percentHeight = 2.5;
        addChild(split);

        autoSize = true;
        layout.refresh();

        disabled = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Displays the comments of the item with the passed id.
     * @param  {Int}  p_id The ID of the item to use.
     * @return {Void}
     */
    public function setItem(p_id : Int) : Void
    {
        _refItem = Globals.allItems.get(p_id);

        // Load the comments
        var user : String = Globals.user;
        var pass : String = Globals.pass;
        var id : Int = _refItem.id;
        var callUrl : String = '${Globals.serverAddress}/themove/getComments.php?user=$user&pass=$pass&id=$id&random=${Math.random()}';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = handleComments;
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(   Main.tongue.get("$COULD_NOT_CONNECT"),
                                                Main.tongue.get("$ERROR") +  " : Comments.setItem");
        };
        http.request(false);

        _commentsContainer.removeAllChildren(true);
        _input.text = "";
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Handles the incoming comments and displays them.
     * @param  {String} p_data The incoming data.
     * @return {Void}
     */
    private function handleComments(p_data : String) : Void
    {
        var comments : Array<Dynamic> = haxe.Json.parse(p_data);
        for (comment in comments)
        {
            var date : Date = Date.fromString(comment.date);
            var entry : CommentEntry = new CommentEntry();
            entry.setup(TextUtils.decodeFromServerFormat(comment.text), comment.accountId, date);
            _commentsContainer.addChild(entry);
        }
        _commentsScroll.vscrollPos = _commentsScroll.vscrollMax;

        disabled = false;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will send the current comment to the server.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleSendComment(p_event : UIEvent) : Void
    {
        // If the text is empty, do nothing
        if (_input.text == "" || _input.text == _lastSentText)
        {
            return;
        }
        _lastSentText = TextUtils.encodeToServerFormat(_input.text);

        // Send it
        var user : String = Globals.user;
        var pass : String = Globals.pass;
        var id : Int = _refItem.id;
        var callUrl : String = '${Globals.serverAddress}/themove/addComment.php?user=$user&pass=$pass&id=$id&text=$_lastSentText';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = function (p_data) {
            setItem(_refItem.id);
            _refItem.numComments++;
            var event : UIEvent = new UIEvent(COMMENT_SENT);
            event.data = _refItem.id;
            dispatchEvent(event);
        }
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(   Main.tongue.get("$COULD_NOT_CONNECT"),
                                                Main.tongue.get("$ERROR") +  " : Comments.handleSendComment");
        };
        http.request(false);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will update the current display, as a removed item possibly changes things here.
     * @param  {Int}  p_id The ID of the item that was removed.
     * @return {Void}
     */
    public function notifyRemovedItem(p_id : Int) : Void
    {
        if (_refItem != null && p_id == _refItem.id)
        {
            _refItem = null;
            _commentsContainer.removeAllChildren(true);
            disabled = true;
        }
    }
}
