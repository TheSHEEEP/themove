package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.text.TextDisplay;
import haxe.ui.toolkit.controls.selection.ListSelector;
import haxe.ui.toolkit.core.StateComponent;
import haxe.ui.toolkit.containers.HSplitter;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.containers.ScrollView;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.core.Component;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ds.IntMap;
import firetongue.FireTongue;

/**
 * User-Status-Part of an ItemListEntry.
 * @type {[type]}
 */
class StatusPart extends HBox
{
    private var _refItem    : Item = null;

    private var _userParts  : IntMap<VBox> = new IntMap<VBox>();

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Setup.
     * @return {Void}
     */
    public function setup(p_item : Item)
    {
        _refItem = p_item;
        verticalAlign = "center";
        horizontalAlign = "center";
        percentWidth = 60.0;
        percentHeight = 100.0;

        // The status for each user
        var isFirst : Bool = true;
        for(accountId in Globals.availableUsers.keys())
        {
            if (Globals.availableUsers.get(accountId) == "Guest") continue;

            // Create a separator
            if (!isFirst)
            {
                var sep : Component = new Component();
                sep.percentHeight = 100.0;
                sep.width = 2;
                sep.id = "vertLine";
                addChild(sep);
            }
            isFirst = false;

            // The name of the user
            var container : VBox = new VBox();
            container.percentWidth = 100.0 / (Globals.numUsers - 1.0);
            container.percentHeight = 100.0;
            container.autoSize = true;
            var userName : Text = new Text();
            userName.percentHeight = 96.0;
            userName.verticalAlign = "center";
            userName.horizontalAlign = "center";
            userName.text = Globals.availableUsers.get(accountId);
            container.addChild(userName);
            var splitter1 : VSplitter = new VSplitter();
            splitter1.percentHeight = 2.0;
            container.addChild(splitter1);
            addChild(container);
            _userParts.set(accountId, container);
            updateUserPart(accountId);
        }

        autoSize = true;
        layout.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Updates the users' parts by updating the BG color / icon.
     * @return {Void}
     */
    public function updateUserParts() : Void
    {
        for (userId in _userParts.keys())
        {
            updateUserPart(userId);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Updates the passed user's part by updating the BG color / icon.
     * @param  {Int}  p_id The ID of the user.
     * @return {Void}
     */
    private function updateUserPart(p_id : Int) : Void
    {
        var part : VBox = _userParts.get(p_id);

        refetchItem();

        // Get the status - use nothing for default
        var status : ItemUserStatus = ItemUserStatus.Nothing;
        if (_refItem.statuses.exists(p_id))
        {
            status = _refItem.statuses.get(p_id);
        }

        // Apply the color
        switch (status)
        {
            case ItemUserStatus.Nothing:
                part.style.backgroundColor = 0xFFFFFF;

            case ItemUserStatus.DoesntWant:
                part.style.backgroundColor = 0xAAFFAA;

            case ItemUserStatus.Wants:
                part.style.backgroundColor = 0xFFAAAA;

            case ItemUserStatus.NotSure:
                part.style.backgroundColor = 0xAAAAFF;

            case ItemUserStatus.Trash:
                part.style.backgroundColor = 0xE56E2D;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Fetches the reference item again.
     * @return {Void}
     */
    public function refetchItem() : Void
    {
        _refItem = Globals.allItems.get(_refItem.id);
    }
}
