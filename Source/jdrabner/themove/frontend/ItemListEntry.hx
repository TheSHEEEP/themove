package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.selection.ListSelector;
import haxe.ui.toolkit.core.StateComponent;
import haxe.ui.toolkit.containers.HSplitter;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.containers.ScrollView;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.core.Component;
import haxe.ui.toolkit.events.UIEvent;
import firetongue.FireTongue;

/**
 * An entry as shown in the ItemList.
 * @type {[type]}
 */
class ItemListEntry extends HBox
{
    private var _refItem    : Item = null;

    private var _name           : Text = null;
    private var _numComments    : Text = null;
    private var _statusPart     : StatusPart = null;

    private var _even       : Bool = false;
    private var _selected   : Bool = false;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
    }


    //------------------------------------------------------------------------------------------------------------------
    /**
     * Setup.
     * @param p_id {Int}    The ID of the item.
     * @return {[type]}
     */
    public function setup(p_id : Int) : Void
    {
        _refItem = Globals.allItems.get(p_id);
        percentWidth = 100.0;
        autoSize = true;
        id = "listEntry";

        // Construct the actual interface
        // Name
        _name = new Text();
        _name.text = _refItem.name;
        _name.clipContent = true;
        _name.horizontalAlign = "left";
        _name.verticalAlign = "center";
        _name.percentWidth = 35.0;
        addChild(_name);

        // Number of comments
        _numComments = new Text();
        _numComments.text = '(${_refItem.numComments})';
        _numComments.horizontalAlign = "center";
        _numComments.verticalAlign = "center";
        _numComments.percentWidth = 5.0;
        addChild(_numComments);

        // Statuses
        _statusPart = new StatusPart();
        _statusPart.setup(_refItem);
        addChild(_statusPart);

        // Add mouse listeners
        addEventListener(UIEvent.CLICK, handleClick);

        autoSize = true;
        layout.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Changes the style depending on if this is an even or odd entry.
     * @param  {Bool} p_even Is this even?
     * @return {Void}
     */
    public function setEven(p_even : Bool) : Void
    {
        _even = p_even;
        if (_selected)
        {
            style.backgroundColor = 0xC9CDC5;
        }
        else
        {
            if (_even)
            {
                style.backgroundColor = 0xE9EDF5;
            }
            else
            {
                style.backgroundColor = 0xFFFFFF;
            }
        }
        layout.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Adjusts the style.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    public function handleMouseOver(p_event : UIEvent) : Void
    {
        if (_even)
        {
            style.backgroundColor = 0xAAAAAA;
        }
        else
        {
            style.backgroundColor = 0xAAAAAA;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Adjusts the style.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    public function handleMouseOut(p_event : UIEvent) : Void
    {
        if (_selected)
        {
            style.backgroundColor = 0xC9CDC5;
        }
        else
        {
            if (_even)
            {
                style.backgroundColor = 0xE9EDF5;
            }
            else
            {
                style.backgroundColor = 0xFFFFFF;
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Adjusts the style.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleClick(p_event : UIEvent) : Void
    {
        style.backgroundColor = 0xC9CDC5;
        _selected = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Adjusts the style.
     * @return {Void}
     */
    public function deselect() : Void
    {
        _selected = false;
        if (_even)
        {
            style.backgroundColor = 0xE9EDF5;
        }
        else
        {
            style.backgroundColor = 0xFFFFFF;
        }
        layout.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Returns the ID of the Item belonging to this entry.
     * @return {Int}
     */
    public function getItemId() : Int
    {
        return _refItem.id;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Updates the display to the current values of the item.
     * @return {Void}
     */
    public function updateDisplay() : Void
    {
        refetchItem();
        _name.text = _refItem.name;
        _numComments.text = '(${_refItem.numComments})';
        _statusPart.updateUserParts();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Fetches the reference item again.
     * @return {Void}
     */
    public function refetchItem() : Void
    {
        _refItem = Globals.allItems.get(_refItem.id);
        _statusPart.refetchItem();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Get the representation of this entry as a HTML string, the row of a table.
     * @return {String}
     */
    public function getHtmlString() : String
    {
        var retVal : String = '';

        // Name
        retVal += '<tr><td>' + _refItem.name + '<br></td>';

        // Status per user
        for (userId in Globals.availableUsers.keys())
        {
            // Username
            var userName : String = Globals.availableUsers.get(userId);
            if (userName == "Guest") continue;
            for (i in 0 ... (15 - userName.length))
            {
                userName += '&nbsp;';
            }

            // Status
            var status : ItemUserStatus = ItemUserStatus.Nothing;
            if (_refItem.statuses.exists(userId))
            {
                status = _refItem.statuses.get(userId);
            }
            var bgColor : String = '#FFFFFF';
            switch (status)
            {
                case ItemUserStatus.NotSure:
                    bgColor = '#AAAAFF';
                case ItemUserStatus.DoesntWant:
                    bgColor = '#AAFFAA';
                case ItemUserStatus.Wants:
                    bgColor = '#FFAAAA';
                default:
                    bgColor = '#FFFFFF';
            }
            retVal += '<td style="background-color: ' + bgColor + '">$userName</td>';
        }
        retVal += '</tr>';

        return retVal;
    }
}
