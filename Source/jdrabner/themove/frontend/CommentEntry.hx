package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import jdrabner.themove.utils.TextUtils;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.containers.ScrollView;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.controls.Divider;
import haxe.ui.toolkit.controls.TextInput;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import firetongue.FireTongue;

/**
 * A single comment entry display.
 * @type {[type]}
 */
class CommentEntry extends VBox
{
    private var _nameLabel  : Text = null;
    private var _dateLabel  : Text = null;
    private var _comment    : Text = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Setup function.
     * @param  {String} p_text   The text to display.
     * @param  {Int}    p_userId The ID of the user that made the comment.
     * @param  {Date}   p_date   The date of posting.
     * @return {[type]}
     */
    public function setup(p_text : String, p_userId : Int, p_date : Date) : Void
    {
        percentWidth = 100.0;
        autoSize = true;

        // Separator
        var sep : Divider = new Divider();
        sep.percentWidth = 100.0;
        addChild(sep);

        // Top bar
        var topBar : HBox = new HBox();
        topBar.percentWidth = 100.0;
        addChild(topBar);

        // Add some space
        var split : VSplitter = new VSplitter();
        split.percentWidth = 100.0;
        split.height = 10.0;
        addChild(split);

        // Name
        _nameLabel = new Text();
        _nameLabel.percentWidth = 50.0;
        _nameLabel.percentHeight = 100.0;
        _nameLabel.textAlign = "left";
        _nameLabel.text = Globals.availableUsers.get(p_userId);
        _nameLabel.style.fontBold = true;
        topBar.addChild(_nameLabel);

        // Date
        var dateContainer : VBox = new VBox();
        dateContainer.percentWidth = 50.0;
        dateContainer.percentHeight = 100.0;
        _dateLabel = new Text();
        _dateLabel.percentHeight = 100.0;
        _dateLabel.textAlign = "right";
        _dateLabel.horizontalAlign = "right";
        _dateLabel.text = DateTools.format(p_date, "%F");
        dateContainer.addChild(_dateLabel);
        topBar.addChild(dateContainer);

        // Comment text
        _comment = new Text();
        _comment.percentWidth = 100.0;
        _comment.multiline = true;
        _comment.wrapLines = true;
        _comment.text = p_text;
        addChild(_comment);
    }
}
