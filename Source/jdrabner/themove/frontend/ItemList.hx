package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.selection.ListSelector;
import haxe.ui.toolkit.core.StateComponent;
import haxe.ui.toolkit.containers.HSplitter;
import haxe.ui.toolkit.containers.VSplitter;
import haxe.ui.toolkit.containers.ScrollView;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.core.interfaces.IDisplayObject;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ui.toolkit.core.PopupManager;
import haxe.ui.toolkit.controls.popups.Popup;
import haxe.ui.toolkit.controls.popups.PopupContent;
import haxe.ui.dialogs.files.FileDetails;
import haxe.ui.dialogs.files.FileDialogs;
import firetongue.FireTongue;
import haxe.ds.IntMap;

/**
 * Displays the list of all known item and offers different sorting possibilities.
 * @type {[type]}
 */
class ItemList extends Container
{
    public static inline var ITEM_SELECTED       : String = "Item selected";

    private var _sortLabel          : Text = null;
    private var _sortSelect         : ListSelector = null;
    private var _lastSortIndex      : Int = 0;

    private var _filterLabel        : Text = null;
    private var _filter             : ListSelector = null;
    private var _lastFilterIndex    : Int = 0;

    private var _scrolledItems      : ScrollView = null;
    private var _scrolledContainer  : VBox = null;

    private var _itemListEntries    : IntMap<ItemListEntry> = new IntMap<ItemListEntry>();
    private var _oldSelectedEntry   : ItemListEntry = null;
    private var _lastHoverEntry     : ItemListEntry = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
        layout = new VerticalLayout();
        percentHeight = 70.0;
        percentWidth = 100.0;

        // Filter, sort and refresh row
        var filterRow : HBox = new HBox();
        filterRow.percentWidth = 100.0;
        filterRow.percentHeight = 10.0;
        filterRow.horizontalAlign = "center";

        // Sort
        _sortLabel = new Text();
        _sortLabel.verticalAlign = "center";
        _sortLabel.text = Main.tongue.get("$SORT");
        _sortSelect = new ListSelector();
        _sortSelect.verticalAlign = "center";
        _sortSelect.dataSource.add({text: Main.tongue.get("$SORT_NEWEST")});
        _sortSelect.dataSource.add({text: Main.tongue.get("$SORT_OLDEST")});
        _sortSelect.dataSource.add({text: Main.tongue.get("$SORT_ALPHABET_ASC")});
        _sortSelect.dataSource.add({text: Main.tongue.get("$SORT_ALPHABET_DESC")});
        _sortSelect.dataSource.add({text: Main.tongue.get("$SORT_STATUS_ASC")});
        _sortSelect.selectedIndex = 0;
        _sortSelect.addEventListener(UIEvent.CHANGE, handleFilterChange);

        // Filter
        _filterLabel = new Text();
        _filterLabel.verticalAlign = "center";
        _filterLabel.text = Main.tongue.get("$FILTER");
        _filter = new ListSelector();
        _filter.verticalAlign = "center";
        _filter.dataSource.add({text: Main.tongue.get("$FILTER_ALL")});
        _filter.dataSource.add({text: Main.tongue.get("$FILTER_CONFLICTS")});
        _filter.dataSource.add({text: Main.tongue.get("$FILTER_NO_CONFLICTS")});
        _filter.dataSource.add({text: Main.tongue.get("$FILTER_AVAILABLE")});
        _filter.dataSource.add({text: Main.tongue.get("$FILTER_DECISION")});
        _filter.selectedIndex = 0;
        _filter.addEventListener(UIEvent.CHANGE, handleFilterChange);

        // Sort & filter row layout
        var splitter : HSplitter = new HSplitter();
        splitter.percentHeight = 100.0;
        _sortLabel.percentWidth = 8.0;
        _sortSelect.percentWidth = 32.0;
        splitter.percentWidth = 20.0;
        _filterLabel.percentWidth = 8.0;
        _filter.percentWidth = 32.0;
        filterRow.addChild(_sortLabel);
        filterRow.addChild(_sortSelect);
        filterRow.addChild(splitter);
        filterRow.addChild(_filterLabel);
        filterRow.addChild(_filter);
        addChild(filterRow);

        // Scrolled items
        _scrolledItems = new ScrollView();
        _scrolledItems.percentWidth = 100.0;
        _scrolledItems.percentHeight = 90.0;
        _scrolledItems.style.backgroundColor = 0xFEFEFE;

        _scrolledContainer = new VBox();
        _scrolledContainer.percentWidth = 100.0;
        _scrolledContainer.style.spacing = 0;
        _scrolledContainer.style.spacingX = 0;
        _scrolledContainer.style.spacingY = 0;
        _scrolledContainer.autoSize = true;
        _scrolledItems.addChild(_scrolledContainer);
        addChild(_scrolledItems);

        autoSize = true;
        layout.refresh();

        addEventListener(UIEvent.MOUSE_MOVE, handleMouseMove);
        addEventListener(UIEvent.MOUSE_OUT, function (p_event) {
            // When not touching anything, deselect
            if (_lastHoverEntry != null)
            {
                _lastHoverEntry.handleMouseOut(p_event);
                _lastHoverEntry = null;
            }
        });
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will get the recent list of items from the server and display them using the current filter.
     * @return {Void}
     */
    public function refresh() : Void
    {
        var callUrl : String = '${Globals.serverAddress}/themove/getAllItems.php?user=${Globals.user}&pass=${Globals.pass}&random=${Math.random()}';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onData = handleItemList;
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(   Main.tongue.get("$COULD_NOT_CONNECT"),
                                                Main.tongue.get("$ERROR") +  " : ItemList.refresh");
        };
        http.request(false);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will display the list of items incoming from the server.
     * @param  {String} p_data The incoming servers.
     * @return {Void}
     */
    private function handleItemList(p_data : String) : Void
    {
        // Returned will be a JSON array of single items
        // To be read into the Globals' list of items
        var entries : Array<Dynamic> = haxe.Json.parse(p_data);
        for (entry in entries)
        {
            Globals.allItems.set(entry.id, Item.fromDynamic(entry));
        }

        refreshDisplay();
        _filter.showList();
        _filter.hideList();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will update the display of the item.
     * @param  {Int}  p_id The ID of the item.
     * @return {Void}
     */
    public function updateItemDisplay(p_id : Int) : Void
    {
        _itemListEntries.get(p_id).updateDisplay();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Refreshes the display by displaying all current items in the currently selected filtering.
     * @return {Void}
     */
    public function refreshDisplay() : Void
    {
        // Create all missing ItemListEntries
        // Also make an array of all item ids
        var list : Array<Item> = new Array<Item>();
        for (itemId in Globals.allItems.keys())
        {
            if (!_itemListEntries.exists(itemId))
            {
                var entry : ItemListEntry = new ItemListEntry();
                entry.addEventListener(UIEvent.CLICK, handleEntryClick);
                entry.setup(itemId);
                _itemListEntries.set(itemId, entry);
            }
            _itemListEntries.get(itemId).updateDisplay();

            // Some filters require that certain items do not make it to the list
            var item : Item = Globals.allItems.get(itemId);
            switch(_filter.selectedIndex)
            {
                // Everything
                case 0:
                    list.push(item);
                // Only conflicts
                case 1:
                    if (item.isConflicted()) list.push(item);
                // No conflicts
                case 2:
                    if (!item.isConflicted()) list.push(item);
                // Only available
                case 3:
                    if (item.isAvailable()) list.push(item);
                // Requiring decision by user
                case 4:
                    if (item.requiresDecision(Globals.userId)) list.push(item);
                default:
                    // Nothing
            }
        }


        // Sort the list either ascending or descending
        switch(_sortSelect.selectedIndex)
        {
            // Oldest first
            case 0:
                list.sort(Item.sortByIdAsc);
            // Newest first
            case 1:
                list.sort(Item.sortByIdDesc);
            // Alphabet ascending
            case 2:
                list.sort(Item.sortByNameAsc);
            // Alphabet descending
            case 3:
                list.sort(Item.sortByNameDesc);
            // By status
            case 4:
                list.sort(Item.sortByStatusDesc);
        }

        // Remove all list items and re-add them in the proper order
        _scrolledContainer.removeAllChildren(false);
        var even : Bool = true;
        for (item in list)
        {
            _scrolledContainer.addChild(_itemListEntries.get(item.id));
            _itemListEntries.get(item.id).setEven(even);
            even = !even;
        }
    }


    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will adjust the styles of the old selected entry and notify the main class of the clicked item.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleEntryClick(p_event : UIEvent) : Void
    {
        if (_oldSelectedEntry != null)
        {
            _oldSelectedEntry.deselect();
        }
        _oldSelectedEntry = cast p_event.displayObject;

        // Notify
        var event : UIEvent = new UIEvent(ITEM_SELECTED);
        event.data = _oldSelectedEntry.getItemId();
        dispatchEvent(event);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will cause hover effects on the items
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleMouseMove(p_event : UIEvent) : Void
    {
        var found : Bool = false;
        for (entry in _itemListEntries)
        {
            var result = entry.findComponentUnderPoint(p_event.stageX, p_event.stageY);
            if (result != null && entry != _lastHoverEntry)
            {
                entry.handleMouseOver(p_event);
                if (_lastHoverEntry != null)
                {
                    _lastHoverEntry.handleMouseOut(p_event);
                }
                _lastHoverEntry = entry;
                found = true;
                break;
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will update the current display, as a removed item possibly changes things here.
     * @param  {Int}  p_id The ID of the item that was removed.
     * @return {Void}
     */
    public function notifyRemovedItem(p_id : Int) : Void
    {
        // Remove the item from the entries
        if (_itemListEntries.exists(p_id))
        {
            var entry : ItemListEntry = _itemListEntries.get(p_id);
            if (_oldSelectedEntry == entry)
            {
                _oldSelectedEntry = null;
            }
            _scrolledContainer.removeChild(entry);
            _itemListEntries.remove(p_id);
        }

        // Make all entries fetch their correct item again
        for (key in _itemListEntries.keys())
        {
            _itemListEntries.get(key).refetchItem();
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will reorder/filter the displayed items.
     * @param  {UIEvent} p_event The event data.
     * @return {Void}
     */
    private function handleFilterChange(p_event : UIEvent) : Void
    {
        // Only do something if there was an actual change
        if (_filter.selectedIndex == _lastFilterIndex &&
            _sortSelect.selectedIndex == _lastSortIndex)
        {
            return;
        }
        _lastFilterIndex = _filter.selectedIndex;
        _lastSortIndex = _sortSelect.selectedIndex;

        refreshDisplay();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will save the currently visible list to a file.
     * @return {Void}
     */
    public function saveCurrent() : Void
    {
        // Create the file
        var details :FileDetails = new FileDetails();
        details.name = "List.html";

        // Write the HTML header
        details.contents = '<!DOCTYPE html>\n';
        details.contents += '<html lang="en">\n';
        details.contents += '  <head> <title>' + Main.tongue.get("$HTML_TITLE") + '</title>\n';
        details.contents += '    <meta charset="utf-8">\n';
        details.contents += '  </head>\n';

        // Write the HTML body
        details.contents += '  <body>\n';
        // Create table of name and user statuses
        details.contents += '    <table>\n';
        details.contents += '      <tr><th>' + Main.tongue.get("$ITEMNAME") + '</th><th>'
                                + Main.tongue.get("$HTML_STATUS") + '</th></tr>\n';
        // Go through each item currently visible
        for (child in _scrolledContainer.get_children())
        {
            var entry : ItemListEntry = cast(child, ItemListEntry);
            details.contents += '      ' + entry.getHtmlString() + '\n';
        }
        details.contents += '    </table>\n';
        details.contents += '  </body>\n';
        details.contents += '</html>\n';

        FileDialogs.saveFileAs(
            {
                dir: "",
                filter: Main.tongue.get("$HTML_FILES") + ":*.html"
            },
            details,
            function(f:FileDetails) {
        });
    }
}
