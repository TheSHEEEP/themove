package jdrabner.themove.frontend;

import jdrabner.themove.data.Globals;
import jdrabner.themove.data.Item;
import jdrabner.themove.utils.TextUtils;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.containers.Container;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.controls.TextInput;
import haxe.ui.toolkit.controls.OptionBox;
import haxe.ui.toolkit.core.Macros;
import haxe.ui.toolkit.core.PopupManager;
import haxe.ui.toolkit.events.UIEvent;
import haxe.ui.toolkit.layout.VerticalLayout;
import haxe.ui.toolkit.layout.HorizontalLayout;
import firetongue.FireTongue;
import haxe.Timer;

/**
 * Displays a single item
 * @type {[type]}
 */
class ItemDetails extends Container
{
    public static inline var ITEM_UPDATED   : String = "Item updated";

    private var _refItem    : Item = null;
    private var _timer      : Timer = null;

    private var _nameBar    : HBox = null;
    private var _nameLabel  : Text = null;
    private var _nameInput  : TextInput = null;

    private var _descBar    : HBox = null;
    private var _descLabel  : Text = null;
    private var _descInput  : TextInput = null;

    private var _statusBar              : HBox = null;
    private var _statusLabel            : Text = null;
    private var _statusOptionsWant      : OptionBox = null;
    private var _statusOptionsDont      : OptionBox = null;
    private var _statusOptionsNoIdea    : OptionBox = null;
    private var _statusOptionsTrash     : OptionBox = null;
    private var _statusOptionsNone      : OptionBox = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        super();
        layout = new VerticalLayout();
        percentHeight = 30.0;
        percentWidth = 100.0;

        // Name
        _nameBar = new HBox();
        _nameBar.percentHeight = 25.0;
        _nameBar.percentWidth = 100.0;
        _nameLabel = new Text();
        _nameLabel.percentWidth = 20.0;
        _nameLabel.text = Main.tongue.get("$ITEMNAME");
        _nameBar.addChild(_nameLabel);
        _nameInput = new TextInput();
        _nameInput.percentWidth = 80.0;
        _nameInput.percentHeight = 95.0;
        _nameInput.text = "";
        _nameBar.addChild(_nameInput);
        addChild(_nameBar);

        // Description
        _descBar = new HBox();
        _descBar.percentHeight = 50.0;
        _descBar.percentWidth = 100.0;
        _descLabel = new Text();
        _descLabel.percentWidth = 20.0;
        _descLabel.text = Main.tongue.get("$ITEMDESC");
        _descBar.addChild(_descLabel);
        _descInput = new TextInput();
        _descInput.percentWidth = 80.0;
        _descInput.percentHeight = 95.0;
        _descInput.multiline = true;
        _descInput.wrapLines = true;
        _descInput.text = "";
        _descBar.addChild(_descInput);
        addChild(_descBar);

        // Status boxes
        _statusBar = new HBox();
        _statusBar.percentHeight = 25.0;
        _statusBar.percentWidth = 100.0;
        _statusLabel = new Text();
        _statusLabel.percentWidth = 20.0;
        _statusLabel.text = Main.tongue.get("$ITEMSTATUS");
        _statusBar.addChild(_statusLabel);
        // Layout - split into two rows
        var statusVBox : VBox = new VBox();
        statusVBox.percentHeight = 100.0;
        statusVBox.percentWidth = 80.0;
        _statusBar.addChild(statusVBox);
        var statusHBox1 : HBox = new HBox();
        var statusHBox2 : HBox = new HBox();
        statusHBox1.percentWidth = statusHBox2.percentWidth = 100.0;
        statusHBox1.percentHeight = statusHBox2.percentHeight = 50.0;
        statusVBox.addChild(statusHBox1);
        statusVBox.addChild(statusHBox2);
        _statusOptionsWant = new OptionBox();
        _statusOptionsWant.verticalAlign = "center";
        _statusOptionsWant.percentWidth = 33.0;
        _statusOptionsWant.text = Main.tongue.get("$STATUS_WANT");
        _statusOptionsWant.group = "options";
        _statusOptionsWant.style.backgroundColor = 0xFFAAAA;
        _statusOptionsDont = new OptionBox();
        _statusOptionsDont.verticalAlign = "center";
        _statusOptionsDont.percentWidth = 33.0;
        _statusOptionsDont.text = Main.tongue.get("$STATUS_DONT");
        _statusOptionsDont.group = "options";
        _statusOptionsDont.style.backgroundColor = 0xAAFFAA;
        _statusOptionsNoIdea = new OptionBox();
        _statusOptionsNoIdea.verticalAlign = "center";
        _statusOptionsNoIdea.percentWidth = 33.0;
        _statusOptionsNoIdea.text = Main.tongue.get("$STATUS_NOIDEA");
        _statusOptionsNoIdea.group = "options";
        _statusOptionsNoIdea.style.backgroundColor = 0xAAAAFF;
        _statusOptionsTrash = new OptionBox();
        _statusOptionsTrash.verticalAlign = "center";
        _statusOptionsTrash.percentWidth = 33.0;
        _statusOptionsTrash.text = Main.tongue.get("$STATUS_TRASH");
        _statusOptionsTrash.group = "options";
        _statusOptionsTrash.style.backgroundColor = 0xE56E2D;
        _statusOptionsNone = new OptionBox();
        _statusOptionsNone.group = "options";
        _statusOptionsNone.visible = false;
        statusHBox1.addChild(_statusOptionsWant);
        statusHBox1.addChild(_statusOptionsDont);
        statusHBox1.addChild(_statusOptionsNoIdea);
        statusHBox2.addChild(_statusOptionsTrash);
        addChild(_statusBar);

        autoSize = true;
        layout.refresh();

        disabled = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Displays the comments of the item with the passed id.
     * @param  {Int}  p_id The ID of the item to use.
     * @return {Void}
     */
    public function setItem(p_id : Int) : Void
    {
        // If an item is currently open, save the changes
        if (_refItem != null)
        {
            save();
        }

        // Get the new item
        _refItem = Globals.allItems.get(p_id);

        // Display its values
        _nameInput.text = _refItem.name;
        _descInput.text = _refItem.description;
        _statusOptionsNone.selected = true;
        if (_refItem.statuses.exists(Globals.userId))
        {
            switch(_refItem.statuses.get(Globals.userId))
            {
                case ItemUserStatus.Wants:
                    _statusOptionsWant.selected = true;
                case ItemUserStatus.DoesntWant:
                    _statusOptionsDont.selected = true;
                case ItemUserStatus.NotSure:
                    _statusOptionsNoIdea.selected = true;
                case ItemUserStatus.Trash:
                    _statusOptionsTrash.selected = true;
                default:
                    // Nothing
            }
        }

        // If not already done, start the timer to save the item once every 3 seconds
        if (_timer == null)
        {
            _timer = new Timer(3000);
            _timer.run = save;
        }

        disabled = false;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Saves the current item values on the server.
     * @return {Void}
     */
    private function save() : Void
    {
        // Store the UI values in the item
        var id : Int = _refItem.id;
        var user : String = Globals.user;
        var pass : String = Globals.pass;
        var name : String = TextUtils.encodeToServerFormat(_nameInput.text);
        var desc : String = TextUtils.encodeToServerFormat(_descInput.text);
        var status : Int = cast (_statusOptionsTrash.selected ? ItemUserStatus.Trash : (
            _statusOptionsWant.selected ? ItemUserStatus.Wants : (
                _statusOptionsDont.selected ? ItemUserStatus.DoesntWant : (
                    _statusOptionsNoIdea.selected ? ItemUserStatus.NotSure : ItemUserStatus.Nothing
            ))));
        _refItem.name = _nameInput.text;
        _refItem.description = _descInput.text;
        _refItem.statuses.set(Globals.userId, cast status);

        // Store on the server
        var callUrl : String = '${Globals.serverAddress}/themove/updateItem.php?user=$user&pass=$pass&id=$id&name=$name&desc=$desc&status=$status';
        var http : haxe.Http = new haxe.Http(callUrl);
        http.onError = function (p_msg : String) : Void {
            PopupManager.instance.showSimple(   Main.tongue.get("$COULD_NOT_CONNECT"),
                                                Main.tongue.get("$ERROR") +  " : ItemDetails.save");
            _timer.stop();
            _timer = null;
        };
        http.request(false);

        // Dispatch the event that this item was (potentially) updated
        var event : UIEvent = new UIEvent(ITEM_UPDATED);
        event.data = id;
        dispatchEvent(event);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will update the current display, as a removed item possibly changes things here.
     * @param  {Int}  p_id The ID of the item that was removed.
     * @return {Void}
     */
    public function notifyRemovedItem(p_id : Int) : Void
    {
        if (_refItem != null && p_id == _refItem.id)
        {
            _refItem = null;
            disabled = true;

            _nameInput.text = "";
            _descInput.text = "";
            _statusOptionsNone.selected = true;
            if (_timer != null)
            {
                _timer.stop();
                _timer = null;
            }
        }
    }
}
