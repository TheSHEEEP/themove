package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;
import jdrabner.themove.data.Item;
import jdrabner.themove.data.Globals;

/**
 * Manages the UpdateItem step.
 * @type {[type]}
 */
class UpdateItem
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("name") && params.exists("user") && params.exists("pass") && params.exists("desc")
                && params.exists("status") && params.exists("id")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        if (user == "Guest")
        {
            Lib.print("Error: Guest cannot update items");
            return;
        }
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }
        var userId : Int = result.next().id;

        // Update the item
        var itemId : Int = Std.parseInt(params.get("id"));
        var name : String = params.get("name");
        var desc : String = params.get("desc");
        var status : ItemUserStatus = cast Std.parseInt(params.get("status"));
        result = _connection.doRequest('UPDATE items SET name="$name", description="$desc" WHERE id=$itemId');
        result = _connection.doRequest('INSERT INTO item_statuses (itemId,accountId,status) VALUES($itemId,$userId,$status) ON DUPLICATE KEY UPDATE itemId=$itemId,accountId=$userId,status=$status');

        Lib.print(haxe.Json.stringify({result: "success"}));
    }
}
