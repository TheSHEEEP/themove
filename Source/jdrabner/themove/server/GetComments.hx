package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.ds.IntMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the GetComments call.
 * @type {[type]}
 */
class GetComments
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("user") && params.exists("pass") && params.exists("id")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }

        // Get all comments of the item
        var itemId : Int = Std.parseInt(params.get("id"));
        result = _connection.doRequest('SELECT * FROM comments WHERE itemId=$itemId ORDER BY date ASC;');
        var comments : Array<Dynamic> = new Array<Dynamic>();
        while (result.hasNext())
        {
            var entry : Dynamic = result.next();
            var comment : Dynamic = {
                id : entry.id,
                accountId : entry.accountId,
                itemId : itemId,
                text : entry.commentText,
                date : entry.date,
            }
            comments.push(comment);
        }
        Lib.print(Json.stringify(comments));
    }
}
