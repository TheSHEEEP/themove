package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.ds.IntMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the GetComments call.
 * @type {[type]}
 */
class AddComment
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("user") && params.exists("pass") && params.exists("id") && params.exists("text")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }
        if (user == "Guest")
        {
            Lib.print('Error: Guests are not allowed to make comments.');
            return;
        }
        var userId : Int = result.next().id;

        // Add a comment
        var itemId : Int = Std.parseInt(params.get("id"));
        var text : String = params.get("text");
        result = _connection.doRequest('INSERT INTO comments (commentText,itemId,accountId) VALUES ("$text",$itemId,$userId);');
        Lib.print("success");
    }
}
