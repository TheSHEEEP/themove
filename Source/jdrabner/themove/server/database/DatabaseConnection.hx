package jdrabner.themove.server.database;

import php.Lib;
import php.Web;
import haxe.ds.IntMap;
import sys.db.Connection;
import sys.db.ResultSet;
import sys.io.File;
import sys.io.FileInput;

/**
 * Helper class to connect to the database.
 * @type {[type]}
 */
class DatabaseConnection
{
    private var _connection : Connection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    public function new()
    {
        // Read the connection file to get user, password, etc.
        // This is just a security measure to not write sensitive data directly in code
        // Make sure this file can't just be downloaded from your server from the outside
        var file : FileInput = null;
        var path : String = '../../../database_demo.txt';
        try
        {
            file = File.read(path, false);
        }
        catch (error : Dynamic)
        {
            Lib.println('Error: $path does not exist. Be sure to create it where the .php scripts are.<br>');
        }
        var address : String = file.readLine();
        var portInt : Int = Std.parseInt(file.readLine());
        var databaseStr : String = file.readLine();
        var userStr : String = file.readLine();
        var passStr : String = file.readLine();

        // Connect to the db
        _connection = sys.db.Mysql.connect({
            host : address,
            port : portInt,
            database : databaseStr,
            user : userStr,
            pass : passStr
        });

        commonStuff();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Workaround function to guarantee that certain methods make it to the lib.
     * @return {[type]}
     */
    private function commonStuff()
    {
        var map : IntMap<String> = new IntMap<String>();
        map.exists(14);
        map.set(4, "4");
        map.get(4);
        haxe.Json.stringify({hello:"world"});
        haxe.Utf8.validate("bla");
        haxe.Utf8.encode("bla");
        Web.getParamsString();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Does the passed request and returns the result.
     * @param  {String}    p_request The SQL request.
     * @return {ResultSet}
     */
    public function doRequest(p_request : String) : ResultSet
    {
        _connection.request("SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci;");
        return _connection.request(p_request);
    }
}
