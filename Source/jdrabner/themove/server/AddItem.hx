package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the AddItem step.
 * @type {[type]}
 */
class AddItem
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("name") && params.exists("user") && params.exists("pass")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        if (user == "Guest")
        {
            Lib.print("Error: Guest cannot add items");
            return;
        }
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }
        var id : Int = result.next().id;

        // Create the item
        var name : String = params.get("name");
        result = _connection.doRequest('INSERT INTO items (name,created_by) VALUES("$name",$id);');
        Lib.print(name);
    }
}
