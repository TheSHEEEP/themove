package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the login step.
 * @type {[type]}
 */
class Login
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Get the params to discover what exactly we are supposed to do
        var params : StringMap<String> = Web.getParams();
        if (!params.exists("do"))
        {
            return;
        }

        php.Lib.toPhpArray([]);

        // Connect to the database
        _connection = new DatabaseConnection();

        // Get the users available
        if (params.get("do") == "getUsers")
        {
            Lib.print(getUsers());
        }
        else if (params.get("do") == "login")
        {
            Lib.print(doLogin());
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Returns the users present in the database as a JSON-Formatted string.
     * @return {String}
     */
    static private function getUsers() : String
    {
        var result : ResultSet = _connection.doRequest("SELECT id, name FROM accounts;");

        var list : Array<Dynamic> = new Array<String>();
        while (result.hasNext())
        {
            var item = result.next();
            list.push({ name: item.name, id: item.id });
        }

        return Json.stringify(list);
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Tries to do a login with the passed user and password.
     * @return {String}
     */
    static private function doLogin() : String
    {
        var params : StringMap<String> = Web.getParams();
        var retVal : Dynamic = null;
        if (!(params.exists("user") && params.exists("pass")))
        {
            retVal = "Error: user or pass param missing";
        }
        else
        {
            var user : String = params.get("user");
            var pass : String = params.get("pass");
            var result : ResultSet = _connection.doRequest('SELECT name FROM accounts WHERE name="$user" AND pass="$pass";');
            retVal = (user == result.next().name);
        }

        return Json.stringify(retVal);
    }
}
