package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.Json;
import jdrabner.themove.data.Globals;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the SetUnsetUnwanted step.
 * @type {[type]}
 */
class SetUnsetUnwanted
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("user") && params.exists("pass")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        if (user == "Guest")
        {
            Lib.print("Error: Guest cannot influence items");
            return;
        }
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }

        // Get a list of all items
        var userId : Int = result.next().id;
        result = _connection.doRequest('SELECT id, item_statuses.status FROM items LEFT JOIN item_statuses ON items.id=item_statuses.itemId AND accountId=$userId;');

        // Go through each item
        while (result.hasNext())
        {
            var entry = result.next();
            var itemId : Int = entry.id;
            var unwantedStatus : Int = cast ItemUserStatus.DoesntWant;

            // If it has a status, set it to unwanted if it isn't set yet
            if (entry.status != null && entry.status == ItemUserStatus.Nothing)
            {
                var innerResult : ResultSet = _connection.doRequest('UPDATE item_statuses SET status = $unwantedStatus WHERE itemId=$itemId;');
            }
            // If it has no status, insert one as unwanted
            else if (entry.status == null)
            {
                var innerResult : ResultSet = _connection.doRequest('INSERT INTO item_statuses (itemId,accountId,status) VALUES($itemId,$userId,$unwantedStatus);');
            }
        }

        Lib.print('success');
    }
}
