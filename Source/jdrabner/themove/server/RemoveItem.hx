package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the RemoveItem step.
 * @type {[type]}
 */
class RemoveItem
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("id") && params.exists("user") && params.exists("pass")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        if (user == "Guest")
        {
            Lib.print("Error: Guest cannot remove items");
            return;
        }
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }

        // Remove all statuses to the item
        var id : Int = Std.parseInt(params.get("id"));
        result = _connection.doRequest('DELETE FROM item_statuses WHERE itemId=$id;');

        // Remove all comments to the item
        result = _connection.doRequest('DELETE FROM comments WHERE itemId=$id;');

        // Remove the item itself
        result = _connection.doRequest('DELETE FROM items WHERE id=$id;');

        // Return JSON-fied item information
        var item : Dynamic = {
            id : id
        };
        Lib.print(Json.stringify(item));
    }
}
