package jdrabner.themove.server;

import php.Lib;
import php.Web;
import sys.db.Connection;
import sys.db.ResultSet;
import haxe.ds.StringMap;
import haxe.ds.IntMap;
import haxe.Json;
import jdrabner.themove.server.database.DatabaseConnection;

/**
 * Manages the GetAllItems call.
 * @type {[type]}
 */
class GetAllItems
{
    static private var _connection : DatabaseConnection = null;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor.
     * @return {[type]}
     */
    static public function main()
    {
        // Check that we have all params
        var params : StringMap<String> = Web.getParams();
        if (!(params.exists("user") && params.exists("pass")))
        {
            Lib.print("Error: Not all parameters.");
        }

        // Connect to the database
        _connection = new DatabaseConnection();

        // Check the user login
        var user : String = params.get("user");
        var pass : String = params.get("pass");
        var result : ResultSet = _connection.doRequest('SELECT id FROM accounts WHERE name="$user" AND pass="$pass";');
        if (!result.hasNext())
        {
            Lib.print('Error: User $user does not exist');
            return;
        }

        // Get all items
        result = _connection.doRequest('SELECT * FROM items LEFT JOIN item_statuses ON items.id=item_statuses.itemId;');
        var items : Array<Dynamic> = new Array<Dynamic>();
        var itemsById : IntMap<Dynamic> = new IntMap<Dynamic>();
        while (result.hasNext())
        {
            var entry : Dynamic = result.next();
            if (!itemsById.exists(entry.id))
            {
                var item : Dynamic = {
                    id : entry.id,
                    name : entry.name,
                    description : entry.description,
                    createdBy : entry.created_by,
                    numComments : 0,
                    statuses : new Array<Dynamic>()
                };
                items.push(item);
                itemsById.set(entry.id, item);
            }
            var item : Dynamic = itemsById.get(entry.id);
            item.statuses.push({id : entry.accountId, status : entry.status});
        }
        // Apply number of comments
        result = _connection.doRequest('SELECT itemId, COUNT(*) as count FROM comments GROUP BY itemId;');
        while (result.hasNext())
        {
            var entry : Dynamic = result.next();
            var id : Int = entry.itemId;
            if (itemsById.exists(id))
            {
                itemsById.get(id).numComments = entry.count;
            }
        }
        Lib.print(Json.stringify(items));
    }
}
