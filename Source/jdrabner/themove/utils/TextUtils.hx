package jdrabner.themove.utils;

/**
 * Utils to help with various text tasks.
 * @type {[type]}
 */
class TextUtils
{
    //------------------------------------------------------------------------------------------------------------------
    /**
     * Encodes the passed string into the server format (mostly to deal with line breaks).
     * @param  {String} p_string The string to encode.
     * @return {String} Returns the encoded string.
     */
    public static function encodeToServerFormat(p_string : String) : String
    {
        var retVal : String = "";
        for (i in 0 ... p_string.length)
        {
            var charCode : Int = p_string.charCodeAt(i);

            // Certain characters cause problems
            if (String.fromCharCode(charCode) == "+")
            {
                retVal += "<plus>";
            }
            else if (String.fromCharCode(charCode) == "\"")
            {
                retVal += "<quote>";
            }
            else
            {
                switch(charCode)
                {
                    // Line break
                    case 13:
                        retVal += "<br>";

                    default:
                        retVal += String.fromCharCode(charCode);
                }
            }
        }
        retVal = StringTools.urlEncode(retVal);
        return retVal;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Decodes the passed string from the server format (mostly to deal with line breaks).
     * @param  {String} p_string The string to decode.
     * @return {String} Returns the decoded string.
     */
    public static function decodeFromServerFormat(p_string : String) : String
    {
        var retVal : String = StringTools.urlDecode(p_string);
        retVal = StringTools.replace(retVal, "<br>", "\n");
        retVal = StringTools.replace(retVal, "<plus>", "+");
        retVal = StringTools.replace(retVal, "<quote>", "\"");
        return retVal;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Will return the string converted to Utf8.
     * @param  {String} p_string The string to convert.
     * @return {String}
     */
    public static function makeUtf8(p_string : String) : String
    {
        var retVal : String = p_string;
        if (!haxe.Utf8.validate(p_string))
        {
            retVal = haxe.Utf8.encode(p_string);
        }
        return retVal;
    }
}
